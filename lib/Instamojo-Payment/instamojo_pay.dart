import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview/Instamojo-Payment/instamojo_helpers/decode_query_params.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_payment_response_model.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_web_payment_view.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/payment_options_model.dart';

/*
InstamojoPay
Gateway create in flutter by wafu Technologies.

Actual Instamojo Gateway will work like this
1.Create payment request with payment options (option must satisfy instamojo gateway requirement)
2.Then instamojo server will issue a payment link to pay.
3.After payment success or failure it will redirect to given webhook with status and paymentid.
4.We can check payment status using payment id.

Flutter Gateaway worklike this:
1.Creates InstamojoPay instance with Apikeys and handlers.
2.calls openCheckout() with current page's Buildcontext and Payment options(which will send as body).
3.that will Request server for new payment.
4.using webview opens that link and in that webview we will pass processAndRoute() a routehandler callback.
5.we will keep track on url's in routehandler and route according to the payment transaction. 

*/

// Note: Please change Instamojo testUrls in Production.

class InstamojoPay {
  final String apiKey;
  final String privateAuthToken;
  final Function(InstamojoPaymentStatusResponse) successHandler;
  final Function(String) failureHandler;
  //test urls
  String requestUrl = "https://test.instamojo.com/api/1.1/payment-requests/";
  String paymentCheckUrl = "https://test.instamojo.com/api/1.1/payments/";

  // String requestUrl = "https://www.instamojo.com/api/1.1/payment-requests/";
  // String paymentCheckUrl = "https://www.instamojo.com/api/1.1/payments/";
  String kAndroidUserAgent =
      "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36";

  Map<String, String> header;

  InstamojoPay(
      {@required this.apiKey,
      @required this.privateAuthToken,
      @required this.successHandler,
      @required this.failureHandler
      });

//It act as mediater between webview and user's called page.
  processAndRoute(
      String url, BuildContext context, bool cancelTransaction) async {
    if (cancelTransaction == true) {
      failureHandler('Transaction cancelled by user');
      Navigator.of(context).pop();
    }
    if (url.contains('payment_id')) {
      try {
        url = url.substring(32); // infuture upate this hard code.
        var result = QueryString.parse(url);
        String statusUrl = "$paymentCheckUrl${result['payment_id']}";
        http.Response statusResponse =
            await http.get(statusUrl, headers: header);
        var decodedBody = json.decode(statusResponse.body);
        if (decodedBody['success'] == true) {
          successHandler(
              InstamojoPaymentStatusResponse.fromJson(decodedBody['payment']));
          Navigator.of(context).pop();
        } else {
          failureHandler('Error in getting status of payment');
          Navigator.of(context).pop();
          throw "Error occured in returning status of payment";
        }
      } catch (e) {
        print("ERROR :: $e");
      }
    }
  }

//It is the entry point of paymentgateway. It will take care of payment process
  openCheckout({InstamojoPaymentOptions options, BuildContext context}) async {
    //set header
    this.header = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json",
      "X-Api-Key": this.apiKey,
      "X-Auth-Token": this.privateAuthToken
    };
    try {
      http.Response reqestPaymentResponce =
          await http.post(requestUrl, headers: header, body: options.toJson());
      if (json.decode(reqestPaymentResponce.body)['success'] == true) {
        String longUrl = json
                .decode(reqestPaymentResponce.body)["payment_request"]
                    ['longurl']
                .toString() +
            "?embed=form";
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InstamojoWebPaymentView(
                title: 'Wafu Payments',
                paymentUrl: longUrl,
                processAndRoute: processAndRoute),
          ),
        );
      }else{
        failureHandler('Unable to create new payment request, please check payment option or try again later');
        throw "Unable to create new payment request to Instamojo, please check the payment options";
      }
    } catch (e) {
      rethrow;
    }
  }

}
