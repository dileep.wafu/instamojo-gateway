import 'package:flutter/foundation.dart';

class InstamojoPaymentStatusResponse {
  String paymentId;
  String status;
  String currency;
  String amount;
  String buyerName;
  String buyerPhone;
  String buyerEmail;
  String instrumentType;
  String billingInstrument;

  InstamojoPaymentStatusResponse(
      {@required this.paymentId,
      @required this.status,
      @required this.currency,
      @required this.amount,
      @required this.buyerName,
      @required this.buyerPhone,
      @required this.buyerEmail,
      @required this.instrumentType,
      @required this.billingInstrument});

  InstamojoPaymentStatusResponse.fromJson(Map<String, dynamic> json) {
    paymentId = json['payment_id'];
    status = json['status'];
    currency = json['currency'];
    amount = json['amount'];
    buyerName = json['buyer_name'];
    buyerPhone = json['buyer_phone'];
    buyerEmail = json['buyer_email'];
    instrumentType = json['instrument_type'];
    billingInstrument = json['billing_instrument'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['payment_id'] = this.paymentId;
    data['status'] = this.status;
    data['currency'] = this.currency;
    data['amount'] = this.amount;
    data['buyer_name'] = this.buyerName;
    data['buyer_phone'] = this.buyerPhone;
    data['buyer_email'] = this.buyerEmail;
    data['instrument_type'] = this.instrumentType;
    data['billing_instrument'] = this.billingInstrument;
    return data;
  }
}