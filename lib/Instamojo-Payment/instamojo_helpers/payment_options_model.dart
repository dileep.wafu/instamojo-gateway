import 'package:flutter/foundation.dart';

class InstamojoPaymentOptions {
  final String amount;
  final String purpose = 'Education';
  final String buyerName;
  final String email;
  final String phone;
  final String allowRepeatedPayments = 'true';
  final String sendEmail = 'false';
  final String sendSms = 'false';
  final String redirectUrl = 'http://www.example.com/redirect/';
  final String webhook = 'http://www.example.com/webhook/';

  InstamojoPaymentOptions(
      {@required this.amount,
      @required this.buyerName,
      @required this.email,
      @required this.phone});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['buyer_name'] = this.buyerName;
    data['amount'] = this.amount;
    data['purpose'] = this.purpose;
    data['send_sms'] = this.sendSms;
    data['send_email'] = this.sendEmail;
    data['redirect_url'] = this.redirectUrl;
    data['webhook'] = this.webhook;
    data['allow_repeated_payments'] = this.allowRepeatedPayments;
    return data;
  }
}
