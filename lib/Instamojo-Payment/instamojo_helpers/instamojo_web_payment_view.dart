import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

/*
InstamojoWebPaymentView::
This webview used to open webUrl that contains payment links.
We will keep track of url's because, on transaction complete Url we will see paymentId.
We pass that url to processAndRoute() along with BuildContext that will take care of rest.
*/

class InstamojoWebPaymentView extends StatefulWidget {
  final String title;
  final String paymentUrl;
  final Function(String, BuildContext, bool) processAndRoute;

  InstamojoWebPaymentView(
      {@required this.title,
      @required this.paymentUrl,
      @required this.processAndRoute});

  @override
  _InstamojoWebPaymentViewState createState() =>
      _InstamojoWebPaymentViewState();
}

class _InstamojoWebPaymentViewState extends State<InstamojoWebPaymentView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                widget.processAndRoute('', context, true);
              })
        ],
      ),
      body: WebView(
        initialUrl: widget.paymentUrl,
        key: UniqueKey(),
        javascriptMode: JavascriptMode.unrestricted,
        onPageStarted: (url) {},
        onPageFinished: (url) {
          widget.processAndRoute(url, context, false);
        },
      ),
    );
  }
}
