import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview/Instamojo-Payment/instamojo_helpers/decode_query_params.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_alert_dialog.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_payment_response_model.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_web_payment_view.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/payment_options_model.dart';

class InstamojoPayWithAlert {
  final String apiKey;
  final String privateAuthToken;
  BuildContext userPageBuildContext;

  //test urls
  String requestUrl = "https://test.instamojo.com/api/1.1/payment-requests/";
  String paymentCheckUrl = "https://test.instamojo.com/api/1.1/payments/";

  // String requestUrl = "https://www.instamojo.com/api/1.1/payment-requests/";
  // String paymentCheckUrl = "https://www.instamojo.com/api/1.1/payments/";
  String kAndroidUserAgent =
      "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36";

  Map<String, String> header;

  InstamojoPayWithAlert({
    @required this.apiKey,
    @required this.privateAuthToken,
  });

//It act as mediater between webview and user's called page.
  processAndRoute(
      String url, BuildContext webViewContext, bool cancelTransaction) async {
    if (cancelTransaction == true) {
      Navigator.of(webViewContext).pop();
      failureHandler('Transaction cancelled by user');
    }
    if (url.contains('payment_id')) {
      try {
        url = url.substring(32); // infuture upate this hard code.
        var result = QueryString.parse(url);
        String statusUrl = "$paymentCheckUrl${result['payment_id']}";
        http.Response statusResponse =
            await http.get(statusUrl, headers: header);
        var decodedBody = json.decode(statusResponse.body);
        if (decodedBody['success'] == true) {
          Navigator.of(webViewContext).pop();
          successHandler(
              InstamojoPaymentStatusResponse.fromJson(decodedBody['payment']));
        } else {
          Navigator.of(webViewContext).pop();
          failureHandler('Error in getting status of payment');
          throw "Error occured in returning status of payment";
        }
      } catch (e) {
        print("$e");
      }
    }
  }

  //It is the entry point of paymentgateway. It will take care of payment process
  openCheckout({InstamojoPaymentOptions options, BuildContext context}) async {
    try {
      //set header
      this.header = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
        "X-Api-Key": this.apiKey,
        "X-Auth-Token": this.privateAuthToken
      };
      this.userPageBuildContext = context;

      http.Response reqestPaymentResponce =
          await http.post(requestUrl, headers: header, body: options.toJson());

      if (json.decode(reqestPaymentResponce.body)['success'] == true) {
        String longUrl = json
                .decode(reqestPaymentResponce.body)["payment_request"]
                    ['longurl']
                .toString() +
            "?embed=form";
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InstamojoWebPaymentView(
                title: 'WAFU Payments',
                paymentUrl: longUrl,
                processAndRoute: processAndRoute),
          ),
        );
      } else {
        failureHandler(
            'Unable to create new payment request, please check payment option or try again later');
        throw 'Unable to create new payment request, please check payment option or try again later';
      }
    } catch (e) {
      rethrow;
    }
  }

  void failureHandler(String message) {
    instamojoAlertDialog(context: userPageBuildContext, message: message);
  }

  void successHandler(
      InstamojoPaymentStatusResponse instamojoPaymentStatusResponse) {
        instamojoAlertDialog(context: userPageBuildContext, message: "Payment Successful");
      }
}
