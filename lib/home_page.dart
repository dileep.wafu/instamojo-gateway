import 'package:flutter/material.dart';
import 'package:webview/Instamojo-Payment/instamojo_pay.dart';
import 'package:webview/Instamojo-Payment/instamojo_pay_alert.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/instamojo_payment_response_model.dart';
import 'package:webview/Instamojo-Payment/instamojo_helpers/payment_options_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // static String apiKey = 'f416d214683642b939c9c311b02e04e7';
  // static String authtoken = '8368447d3f34aeb69f99445af7ec27ff';

  //testapi
  static String apiKey = 'test_a0d3653a8764c6e5ba21a31fc25';
  static String authtoken = 'test_566886506a6ff884d53aa8e0bde';

  // InstamojoPay instamojo;

  successHandler(InstamojoPaymentStatusResponse response) {
    print("Success : From home page");
    print("${response.toJson()}");
  }

  failureHandler(String message) {
    print(message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payments"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text("Open payment 15rs"),
              onPressed: () async {
                InstamojoPaymentOptions options =InstamojoPaymentOptions(amount: '15', buyerName: 'Cool', email: 'tester@gmail.com',phone: '+916304502305' );
                try {
                  InstamojoPay instamojo = InstamojoPay(apiKey: apiKey, privateAuthToken: authtoken, successHandler: successHandler, failureHandler: failureHandler);
                  instamojo.openCheckout(context: context,options: options);
                } catch (e) {
                  print("@@GOT ERROR :: $e");
                }
              },
            ),
                        RaisedButton(
              child: Text("Open payment 10rs"),
              onPressed: () async {
                InstamojoPaymentOptions options =InstamojoPaymentOptions(amount: '10', buyerName: 'Cool', email: 'tester@gmail.com',phone: '+916304502305' );
                try {
                  InstamojoPay instamojo = InstamojoPay(apiKey: apiKey, privateAuthToken: authtoken, successHandler: successHandler, failureHandler: failureHandler);
                  instamojo.openCheckout(context: context,options: options);
                } catch (e) {
                  print("@@GOT ERROR :: $e");
                }
              },
            ),
                                    RaisedButton(
              child: Text("Open payment 20rs"),
              onPressed: () async {
                InstamojoPaymentOptions options =InstamojoPaymentOptions(amount: '20', buyerName: 'Cool', email: 'tester@gmail.com',phone: '+916304502305' );
                try {
                  InstamojoPayWithAlert instamojo = InstamojoPayWithAlert(apiKey: apiKey, privateAuthToken: authtoken);
                  instamojo.openCheckout(context: context,options: options);
                } catch (e) {
                  print("@@GOT ERROR :: $e");
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
